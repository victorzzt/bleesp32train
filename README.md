### BLEEsp32Train Code And Gerber

* As it's a test, all hard coded
* The ESP32 DEV-kit used is with only 2 mounting screws [MH-ET Live](https://www.epicworldstore.com/products/mh-et-live-esp32-development-board-wifi-bluetooth-ultra-low-power-consumption-dual-core-esp-32-esp-32s-esp-32-similar-esp8266)
    * Need to order without pins soldered, as you only have space to use thin wirewrap cable
	* It is possible to modify using the DEVKit V1
* To use this design with Controller board, you will need to fabricate a mini motor driver board which is in this repository
    * The original project was drawn in DesignSpark
    * To fabricate, just use the gerber file in /plot in the repository
* The toy train works, however, the printed wheel doesn't have enough traction [Watch on youtube](https://www.youtube.com/watch?v=28MwiZmm1T4)

* Use a ble terminal like: [This one](https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal)
    * forward - 0xA0
    * backward - 0xA1
    * stop - 0xB0

* Code (Modified from BLE example from ESP32 Arduino Core):

```
// #include <sigmadelta.h>
 
// BLE
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
 
const int Motor_PINS[] = {2, 4};
const int Motor_Freqs[] = {312500, 312500};
 
// BLE
BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;
// uint8_t txValue = 0;
// TestCharStr
uint8_t txValue[] = "I'm Alive\n";
// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
 
class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };
 
    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};
 
class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();
 
      if (rxValue.length() > 0) {
        Serial.print("Received Value: ");
        for (int i = 0; i < rxValue.length(); i++) {
          Serial.print(rxValue[i]);
        }
        switch ( rxValue[0] ) {
          case 0xB0:
            Serial.print(" Stp");
            for ( int i = 0; i < 2; i++ ) {
              sigmaDeltaWrite(i, 0);  // turn off all
            }
            break;
          case 0xA0:
            sigmaDeltaWrite(0, 0);  // off 0
            sigmaDeltaWrite(1, 255);  // on 1
            Serial.print(" Bck");
            break;
          case 0xA1:
            sigmaDeltaWrite(1, 0);  // off 1
            sigmaDeltaWrite(0, 255);  // on 0
            Serial.print(" Fwd");
            break;
          default:
            Serial.print(" Nothing to do");
            break;
        }
        Serial.println();
      }
    }
};
 
 
void setup() {
  Serial.begin(115200);
 
  Serial.print("SigmaDelta Initializing:");
  for ( int i = 0; i < 2; i++ ) {
    Serial.print(".");
    sigmaDeltaSetup(i, Motor_Freqs[i]);
    sigmaDeltaAttachPin(Motor_PINS[i], i );
    sigmaDeltaWrite(i, 0);  // turn off all
  }
 
  Serial.println("Done");
 
  // Now hard coded
  Serial.print("Initialize BLE:");
  BLEDevice::init("TrainMakerfaire");
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());
 
  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);
  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
                        CHARACTERISTIC_UUID_TX,
                        BLECharacteristic::PROPERTY_NOTIFY
                      );
  pTxCharacteristic->addDescriptor(new BLE2902());
 
  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
      CHARACTERISTIC_UUID_RX,
      BLECharacteristic::PROPERTY_WRITE
                                          );
 
  pRxCharacteristic->setCallbacks(new MyCallbacks());
 
  // Start the service
  pService->start();
 
  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");
}
 
void loop() {
  if (deviceConnected) {
    pTxCharacteristic->setValue(txValue, sizeof(txValue));
    pTxCharacteristic->notify();
    // txValue++;
    delay(1000); // bluetooth stack will go into congestion, if too many packets are sent
  }
 
  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    Serial.println("Waiting for new connections");
    oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;
  }
}
```